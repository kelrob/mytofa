<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('product_name')->nullable();
            $table->integer('price_per_tonne')->default(0);
            $table->integer('loading_expenses')->default(0);
            $table->integer('paper_expenses')->default(0);
            $table->integer('transportation_cost')->default(0);
            $table->integer('tonnage')->default(0);
            $table->integer('quality_check')->nullable();
            $table->integer('accommodation')->default(0);
            $table->integer('quantity')->default(0);
            $table->string('volume')->nullable();
            $table->string('transaction_type')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('selling_price', 10, 2)->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('buyer_name')->nullable();
            $table->decimal('cost_of_funds', 10, 2)->default(0);
            $table->boolean('profit')->nullable();
            $table->string('buyer_phone')->nullable();
            $table->string('seller_phone')->nullable();
            $table->boolean('loss')->nullable();
            $table->string('truck_driver_no', 20)->nullable();
            $table->string('truck_driver_name')->nullable();
            $table->string('truck_plate_no', 20)->nullable();
            $table->string('expected_close_date')->nullable();
            $table->integer('buyer_payment_status')->default(0);
            $table->integer('supplier_payment_status')->default(0);
            $table->string('transaction_code')->nullable();
            $table->integer('transaction_status')->nullable();
            $table->integer('funded_by')->nullable();
            $table->string('pickup_location')->nullable();
            $table->string('delivery_location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
