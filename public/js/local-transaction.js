const format = num => String(num).replace(/(?<!\..*)(\d)(?=(?:\d{3})+(?:\.|$))/g, '$1,');

// Add Commas to the Input Number
$(document).ready(function(){
    $("input[data-type='number']").keyup(function(event){
        // skip for arrow keys
        if(event.which >= 37 && event.which <= 40){
            event.preventDefault();
        }
        var $this = $(this);
        var num = $this.val().replace(/,/gi, "");
        var num2 = num.split(/(?=(?:\d{3})+$)/).join(",");
        //console.log(num2);
        // the following line has been simplified. Revision history contains original.
        $this.val(num2);
    });
});

// Calculate Landing Cost
const calculateLandingCost = () => {
    let accommodationCost = $('#accommodation_cost').val();
    let transportCost = $('#transportation_cost').val();
    let loadingExpenses = $('#loading_expenses').val();
    let paperExpenses = $('#paper_expenses').val();
    let sellingPrice = $('#selling_price').val();
    let qualityCheck = $('#quality_check').val();
    let productName = $('#product_name').val();
    let pricePerKG = $('#price_per_kg').val();
    let quantity = $('#quantity').val();
    let tonnage = $('#tonnage').val();
    let volume = $('#volume').val();

    if (accommodationCost >= 0 || transportCost >= 0 || loadingExpenses >= 0 || paperExpenses >= 0 || pricePerKG >= 0 || tonnage >= 0) {

        loadingExpenses = Number(loadingExpenses.replace(/\,/g, ''));
        transportCost = Number(transportCost.replace(/\,/g, ''));
        paperExpenses = Number(paperExpenses.replace(/\,/g, ''));
        pricePerKG = Number(pricePerKG.replace(/\,/g, ''));
        tonnage = Number(tonnage.replace(/\,/g, ''));
        accommodationCost = Number(accommodationCost.replace(/\,/g, ''));
        qualityCheck = qualityCheck != '' ? Number(qualityCheck.replace(/\,/g, '')) : 0;

        // Calculate Cost of Funds
        let costOfFunds = ((transportCost + loadingExpenses + paperExpenses + qualityCheck + accommodationCost) / (tonnage) + pricePerKG);

        // Display Cost of funds
        $('#cost_of_funds').val(Number(costOfFunds.toFixed(2)).toFixed(2));
    } else {
        console.log(tonnage);
        $('#cost_of_funds').val('calculating cost of funds...');
    }

    if (accommodationCost >= 0 && transportCost >= 0 && loadingExpenses >= 0 && paperExpenses >= 0 && sellingPrice >= 0 && pricePerKG >= 0 && tonnage >= 0 && productName != '') {
        $('#analyze_transaction').removeAttr('disabled');
    } else {
        $('#analyze_transaction').removeAttr('disabled');
    }
}

// Ananlyze Profit or Loss
const analyzeTransaction = () => {
    alert('Analyze Transaction')
}

// Show Info on Modal
const showInfo = () => {

    // Get Inputted Value
    let transportCost = $('#transportation_cost').val();
    let loadingExpenses = $('#loading_expenses').val();
    let paperExpenses = $('#paper_expenses').val();
    let pricePerKG = $('#price_per_kg').val();
    let tonnage = $('#tonnage').val();
    let sellingPrice = $('#selling_price').val();
    let productName = $('#product_name').val();
    let costOfFunds = $('#cost_of_funds').val();
    let qualityCheck = $('#quality_check').val();
    let accomodationCost = $('#accomodation_cost').val();

    // Show the values in the modal
    $('#transportation_cost_value').html(transportCost);
    $('#loading_expenses_value').html(loadingExpenses);
    $('#paper_expenses_value').html(paperExpenses);
    $('#price_per_kg_value').html(pricePerKG);
    $('#tonnage_value').html(tonnage);
    $('#selling_price_value').html(sellingPrice);
    $('#product_name_value').html(productName);
    $('#cost_of_funds_value').html(costOfFunds);

    // Determine Profit or loss
    let profitValue;
    let lossValue;
    let displayMessage;

    costOfFunds = Number(costOfFunds.replace(/\,/g, ''));
    sellingPrice = Number(sellingPrice.replace(/\,/g, ''));

    if (sellingPrice > costOfFunds) {
        profit = true;
        profitValue = sellingPrice - costOfFunds;
        profitValue = Number(profitValue.toFixed(2)).toFixed(2)

        displayMessage = `
            <div class="text-success">Profit <h3 class="text-success">&#8358;${format(profitValue)}</h3></div>
        `;

        $('#display_message_value').html(displayMessage);
    } else if (sellingPrice < costOfFunds) {
        profit = false;
        lossValue = sellingPrice - costOfFunds;
        lossValue = Number(lossValue.toFixed(2)).toFixed(2)

        displayMessage = `
            <div class="text-danger">Loss <h3 class="text-danger">&#8358; ${format(lossValue)}</div>
        `;

        $('#display_message_value').html(displayMessage);
    }

}

// Transaction form submit
const transactionFormSubmit = () => {
    $('#proceed_btn').html('Please wait...').attr("disabled", "disabled");
    $('#local_transaction_form').submit();
}
