<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'Auth\LoginController@index');


# Public Route
Route::get('/accept-invite/{email}/{token}', 'SuperAdmin\PagesController@acceptInvite');
Route::post('/complete-signup', 'PublicController@completeSignUp');
Route::get('/401', 'PublicController@errorCode401');

# Super Admin Routes
Route::middleware(['super_admin'])->group(function () {
    Route::get('/search/staff', 'SuperAdmin\StaffController@searchStaff');
    Route::get('/search/transaction', 'SuperAdmin\TransactionController@searhTransaction');
    Route::get('/add-staff', 'SuperAdmin\PagesController@createStaff')->name('create-staff');
    Route::get('/dashboard', 'SuperAdmin\HomeController@index')->name('dashboard');
    Route::get('/staffs', 'SuperAdmin\PagesController@staffs')->name('staffs');
    Route::get('/staff/{id}', 'SuperAdmin\PagesController@viewStaff');
    Route::post('/new-staff', 'SuperAdmin\StaffController@newStaff');
    Route::get('/transactions', 'SuperAdmin\PagesController@transactions');
    Route::get('/staff-transactions/{staffId}', 'SuperAdmin\PagesController@staffTransactions');
    Route::get('/disable-staff/{staffId}/{disabledStatus}', 'SuperAdmin\StaffController@disableStaff');
});

# Agro-Commodity Routes
Route::middleware(['agro_commodity'])->group(function () {
    Route::get('/home', 'AgroCommodity\PagesController@index')->name('home');
    Route::get('/my-profile', 'AgroCommodity\PagesController@myProfile')->name('my-profile');
    Route::post('/update-profile', 'SuperAdmin\StaffController@editProfile');
    Route::post('/change-password', 'SuperAdmin\StaffController@changePassword');
    Route::get('/my-transactions', 'AgroCommodity\PagesController@myTransactions')->name('my-transactions');
    Route::get('/start-local-transaction', 'AgroCommodity\PagesController@createLocalTransaction');
    Route::post('/create-local-transaction', 'AgroCommodity\TransactionController@createLocalTransaction');
    Route::get('/process-local-transaction/{transaction_code}', 'AgroCommodity\PagesController@processLocalTransaction');
    Route::post('/process-local-transaction/', 'AgroCommodity\TransactionController@processLocalTransaction');
    Route::get('/cancel-local-transaction/{transactionCode}', 'AgroCommodity\TransactionController@cancelLocalTransaction');
    Route::post('/update-local-transaction/{transactionCode}', 'AgroCommodity\TransactionController@updateLocalTransaction');
    Route::get('/search/my-transaction', 'AgroCommodity\TransactionController@searchMyTransactions');
    Route::get('/edit-local-transaction/{transactionCode}', 'AgroCommodity\PagesController@editLocalTransaction');
    
});

Route::middleware(['finance'])->group(function () {
    Route::get('/account', 'Finance\PagesController@index')->name('account');
    Route::get('/fund-transaction/{transactionCode}', 'Finance\TransactionController@fundTransaction');
    Route::get('/funded-transactions', 'Finance\PagesController@fundedTransactions');
    Route::get('/non-funded-transactions', 'Finance\PagesController@nonFundedTransactions');
});

# All role routes
Route::middleware(['multiple_roles'])->group(function () {
    Route::get('/search/transaction', 'SuperAdmin\TransactionController@searhTransaction');
    Route::get('/completed-local-transaction/{transactionCode}', 'AgroCommodity\PagesController@completeLocalTransaction');
    Route::get('/generate-deal-memo-excel/{transactionCode}', 'AgroCommodity\TransactionController@dealMemoExcel');
    Route::get('/notification/view/{id}', 'AgroCommodity\PagesController@viewNotifications');
});
