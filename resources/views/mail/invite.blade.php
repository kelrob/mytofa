<style>
    body {
        font-family: Arial, serif;
        background-color: #eee;
    }
</style>
<body style="background-color: #eee; font-family: Arial;">
<div class="container">
    <div class="row" style="padding: 3%;" align="center">
        <div class="col-lg-6 p-3" style="background-color: #fff; padding: 2%; width: 88%;">
            <div align="center" style="border-bottom: 1px solid #eee; padding-bottom: 4%;">
                <img src="https://tradersofafrica.com/main/img/logo.png" style="max-width: 15%;" alt="TOFA">
            </div>

            <h4>{{ $name }}</h4>
            <p>You have been invited as a staff to join <b>MyTOFA</b>.</p>
            <p>You can choose to Ignore this request or Accept this request by clicking on the button below.</p>
            <p style="margin-top: 5%;">TEAM AWESOME</p>
            <p style="margin-top: 5%; width: 100%;">
                <a href="{{ url('/accept-invite/' . $email . '/' . $token) }}"
                   style=" width: 100%; border-radius: 25px; background-color: #248afd; color: #fff; text-decoration: none; padding: 1.5% 15%;">Accept
                    Request</a>
            </p>
            <p class="text-center" style="margin-top: 5%;">
                <small>&copy; Copyright {{ date('Y')}} All right reserved.</small>
            </p>
        </div>
        <div class="col-lg-3 text-center"></div>
    </div>
</div>
</body>
