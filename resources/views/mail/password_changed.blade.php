<style>
    body {
        font-family: Arial, serif;
        background-color: #eee;
    }

</style>

<body style="background-color: #eee; font-family: Arial;">
    <div class="container">
        <div class="row" style="padding: 3%;" align="center">
            <div class="col-lg-6 p-3" style="background-color: #fff; padding: 2%; width: 88%;">
                <div align="center" style="border-bottom: 1px solid #eee; padding-bottom: 4%;">
                    <img src="https://tradersofafrica.com/main/img/logo.png" style="max-width: 15%;" alt="TOFA">
                </div>

                <h4>Hello {{ $name }},</h4>
                <p>This is a notification to let you know that your password has just been changed on <b>MyTOFA</b>.</p>
                <p>Please if you did not make this change kindly contact us immediately.</p>
                <p style="margin-top: 5%;">TEAM AWESOME</p>

                <p class="text-center" style="margin-top: 5%;">
                    <small>&copy; Copyright {{ date('Y') }} All right reserved.</small>
                </p>
            </div>
            <div class="col-lg-3 text-center"></div>
        </div>
    </div>
</body>
