<nav class="sidebar sidebar-offcanvas" id="sidebar" style="background-color: #fff;">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('home') }}">
                <i class="ti-shield menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('my-transactions') }}">
                <i class="ti-shopping-cart-full menu-icon"></i>
                <span class="menu-title">Transactions</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <i class="ti-power-off menu-icon"></i>
                <span class="menu-title">Logout</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
    </ul>
</nav>
