<!-- partial:partials/_navbar.html -->
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo mr-5" href="{{ url('/dashboard') }}">
            <img src="{{ url('/images/logo.png') }}" style="max-width: 65%;" class="mr-2" alt="logo" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="{{ url('/account') }}">
            <img src="https://tradersofafrica.com/main/img/fav.png" style="max-width: 65%;" alt="logo" /></a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="ti-view-list"></span>
        </button>
        <ul class="navbar-nav mr-lg-2">
            <li class="nav-item nav-search d-none d-lg-block">
                <h5 class="mt-2">
                    Good
                    {{ ucwords(getDayTime()) . ', ' . Auth::user()->name }}
                </h5>
            </li>
            {{-- <li class="nav-item nav-search d-none d-lg-block">
                --}}
                {{-- <div class="input-group">--}}
                    {{-- <div class="input-group-prepend hover-cursor"
                        id="navbar-search-icon">--}}
                        {{-- <span class="input-group-text"
                            id="search">--}}
                            {{-- <i class="ti-search"></i>--}}
                            {{-- </span>--}}
                        {{-- </div>--}}
                    {{-- <input type="text" class="form-control" id="navbar-search-input"
                        placeholder="Search now" aria-label="search"
                        aria-describedby="search">--}}
                    {{-- </div>--}}
                {{-- </li>--}}
        </ul>
        <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item dropdown">
                <a class="nav-link {{ notificationExixt(Auth::user()->id) ? 'dropdown-toggle count-indicator' : '' }}"
                    id="notificationDropdown" href="#" data-toggle="dropdown">
                    <i class="ti-bell mx-0"></i>
                    <span class="count"></span>
                </a>

                @if (notificationExixt(Auth::user()->id))
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown"
                        aria-labelledby="notificationDropdown">
                        <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>

                        @foreach (getNotifications(Auth::user()->id) as $notification)
                            <a class="dropdown-item"
                                href="{{ url('/completed-local-transaction/' . $notification->transaction_code . '?ref=nfn&key=' . $notification->id) }}">
                                <div class="item-thumbnail">
                                    <div class="item-icon bg-primary">
                                        <i class="ti-truck mx-0"></i>
                                    </div>
                                </div>
                                <div class="item-content">
                                    <h6
                                        class="{{ $notification->is_read == 0 ? 'font-weight-bold ' : 'font-weight-normal' }}">
                                        {{ truncate($notification->message) }}</h6>
                                    <p
                                        class="{{ $notification->is_read == 0 ? 'font-weight-bold ' : 'font-weight-light' }} small-text mb-0 text-muted">
                                        {{ date('M d, Y - H:ia', strtotime($notification->created_at)) }}

                                    </p>
                                </div>
                            </a>
                        @endforeach

                    </div>
                @endif
            </li>
            <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                    <img src="{{ url('images/dashboard/avatar.png') }}" alt="profile" />
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                    <a class="dropdown-item" href="{{ url('my-profile') }}">
                        <i class="ti-settings text-primary"></i>
                        Profile
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="ti-power-off text-primary"></i>
                        Logout

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </a>
                </div>
            </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
            data-toggle="offcanvas">
            <span class="ti-view-list"></span>
        </button>
    </div>
</nav>
<!-- partial -->
