@extends('layouts.agro-commodity-template')
@section('content-wrapper')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card position-relative">
                    <div class="card-body">
                        <div class="row p-3">
                            <div class="col-md-6">
                                <h4 id="lt-sp-1" class="font-weight-bold">Non Funded Transactions</h4>
                            </div>
                            <div class="col-md-6">
                                <form action="{{ url('/search/transaction') }}" method="GET">
                                    <div class="input-group md-form form-sm form-2 pl-0">
                                        <input class="form-control my-0 py-1 red-border" type="text"
                                            placeholder="Search Transaction Code" name="search" aria-label="Search">
                                        <div class="input-group-append">
                                            <button type="submit" class="p-0 border-0">
                                                <span class="input-group-text red lighten-3" id="basic-text1"><i
                                                        class="ti ti-search text-grey" aria-hidden="true"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12 mt-4">
                                <div class="table-responsive">
                                    <table class="table table-hover mb-2">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Staff</th>
                                                <th>Ref No</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Date Created</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($transactions as $transaction)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $transaction->user->name }}</td>
                                                    <td>{{ $transaction->transaction_code }}</td>
                                                    <td>
                                                        {{ $transaction->quantity . ' ' . $transaction->volume . ' of ' . $transaction->product_name }}
                                                    </td>
                                                    <td>
                                                        @if ($transaction->transaction_status == null)
                                                            <small>
                                                                <span class="bg-danger p-1 text-white rounded">
                                                                    Not funded
                                                                </span>
                                                            </small>
                                                        @else
                                                            <small>
                                                                <span class="bg-success p-1 text-white rounded">
                                                                    Funded
                                                                </span>
                                                            </small>
                                                        @endif
                                                    </td>
                                                    <td>{{ date('M d, Y', strtotime($transaction->created_at)) }}</td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <button class="btn btn-light btn-sm" type="button"
                                                                id="actionMenu" data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                                <i class="ti-more"></i>
                                                            </button>
                                                            @if ($transaction->transaction_type == 'local')
                                                                <div class="dropdown-menu shadow bg-white rounded"
                                                                    id="more-action" aria-labelledby="actionMenu">
                                                                    <a class="dropdown-item"
                                                                        href={{ url('completed-local-transaction/' . $transaction->transaction_code) }}>
                                                                        View Transaction
                                                                    </a>

                                                                </div>
                                                            @elseif($transaction->transaction_type ==
                                                                'international')
                                                                <div class="dropdown-menu shadow bg-white rounded"
                                                                    id="more-action" aria-labelledby="actionMenu">
                                                                    <a class="dropdown-item"
                                                                        href={{ url('completed-international-transaction/' . $transaction->transaction_code) }}>
                                                                        View Transaction
                                                                    </a>

                                                                </div>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{ $transactions->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
