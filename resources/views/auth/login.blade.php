<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>My Tofa</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ url('/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ url('/vendors/base/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ url('/css/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" type="image/png" href="{{ url('https://tradersofafrica.com/main/img/fav.png') }}">

    <link href="https://fonts.googleapis.com/css2?family=Khula&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Khula', sans-serif;
        }

    </style>
</head>

<body>
    <div class="container-scroller" id="body">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
                <div class="row flex-grow">
                    <div class="col-lg-6 d-flex align-items-center justify-content-center bg-white">
                        <div class="auth-form-transparent text-left p-3">
                            <div class="brand-logo">
                                <img src="{{ url('/images/logo.png') }}" alt="logo">
                            </div>

                            @if (request()->get('auth') == 'success')
                                <h4 class="text-success font-weight-bold">Account Setup Complete! <span
                                        class="ti-check"></span></h4>
                                <h6 class="font-weight-light">Now you can Login!</h6>
                            @else
                                <h4>Welcome back!</h4>
                                <h6 class="font-weight-light">Happy to see you again!</h6>
                            @endif
                            <form class="pt-3" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail">Email</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend bg-transparent">
                                            <span class="input-group-text bg-transparent border-right-0">
                                                <i class="ti-user text-primary"></i>
                                            </span>
                                        </div>
                                        <input type="text"
                                            class="form-control form-control-lg border-left-0 @error('email') is-invalid @enderror"
                                            name="email" value="{{ old('email') }}" autocomplete="email" autofocus
                                            id="exampleInputEmail" placeholder="Email">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong class="text-danger">{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword">Password</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend bg-transparent">
                                            <span class="input-group-text bg-transparent border-right-0">
                                                <i class="ti-lock text-primary"></i>
                                            </span>
                                        </div>
                                        <input type="password"
                                            class="form-control form-control-lg border-left-0 @error('password') is-invalid @enderror"
                                            name="password" autocomplete="current-password" id="exampleInputPassword"
                                            placeholder="Password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong class="text-danger">{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="my-2 d-flex justify-content-between align-items-center">
                                    <div class="form-check">
                                        {{-- <label
                                            class="form-check-label text-muted">--}}
                                            {{-- <input type="checkbox"
                                                class="form-check-input">--}}
                                            {{-- Keep me signed
                                            in--}}
                                            {{-- </label>--}}
                                    </div>
                                    <a href="{{ url('password/reset') }}" class="auth-link text-black">Forgot
                                        password?</a>
                                </div>
                                <div class="my-3">
                                    <button type="submit"
                                        class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">
                                        LOGIN
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 login-half-bg d-flex flex-row">
                        <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright
                            &copy; {{ date('Y') }} All rights reserved.</p>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ url('/vendors/base/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="{{ url('/js/off-canvas.js') }}"></script>
    <script src="{{ url('/js/hoverable-collapse.js') }}"></script>
    <script src="{{ url('/js/template.js') }}"></script>
    <script src="{{ url('/js/todolist.js') }}"></script>
    <!-- endinject -->
</body>

</html>
