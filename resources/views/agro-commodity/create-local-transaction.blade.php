@extends('layouts.agro-commodity-template')

@section('content-wrapper')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card position-relative">
                    <div class="card-body">
                        <div class="row" id="new-transaction-row">
                            <div class="col-md-12 pl-3 pt-3 pr-3 pb-0">
                                <h4 id="lt-sp-1" class="font-weight-bold">Starting a new Local Transaction</h4>
                            </div>
                            <div class="col-md-12">
                                <form method="POST" action="{{ url('/create-local-transaction') }}" id="local_transaction_form">
                                    @csrf
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="product_name">Product Name</label>
                                            <input type="text" name="product_name" value="{{ old('product_name') }}" id="product_name" class="form-control" onkeyup="calculateLandingCost()">
                                            
                                            @error('product_name')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="price_per_kg">Price Per KG</label>
                                            <input type="text" data-type="number" name="price_per_kg" value="{{ old('price_per_kg') }}" id="price_per_kg" class="form-control" onkeyup="calculateLandingCost()">
                                            
                                            @error('price_per_kg')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="loading_expenses">Loading Expenses</label>
                                            <input type="text" data-type="number" name="loading_expenses" value="{{ old('loading_expenses') }}" id="loading_expenses" class="form-control" onkeyup="calculateLandingCost()">
                                        
                                            @error('loading_expenses')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="paper_expenses">Paper Expenses</label>
                                            <input type="text" data-type="number" name="paper_expenses" value="{{ old('paper_expenses') }}" id="paper_expenses" class="form-control" onkeyup="calculateLandingCost()">
                                            
                                            @error('paper_expenses')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="transportation_cost">Transportation Cost</label>
                                            <input type="text" data-type="number" name="transportation_cost" onkeyup="calculateLandingCost()" value="{{ old('transportation_cost') }}" id="transportation_cost" class="form-control">
                                            
                                            @error('transportation_cost')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="tonnage">Tonnage <small>(Total No of Tonne / Truck)</small></label>
                                            <input type="text" data-type="number" name="tonnage" onkeyup="calculateLandingCost()" value="{{ old('tonnage') }}" id="tonnage" class="form-control">
                                        
                                            @error('tonnage')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="quality_check">Quality Check (QC)</label>
                                            <input type="text" data-type="number" name="quality_check" onkeyup="calculateLandingCost()" value="{{ old('quality_check') }}" id="quality_check" class="form-control">
                                            
                                            @error('quality_check')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="accommodation_cost">Hotel / Accomodation Cost</label>
                                            <input type="text" data-type="number" name="accommodation_cost" onkeyup="calculateLandingCost()" value="{{ old('accommodation_cost') }}" id="accommodation_cost" class="form-control">
                                        
                                            @error('accommodation_cost')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="quantity">Quantity</label>
                                            <input type="text" data-type="number" name="quantity" onkeyup="calculateLandingCost()" value="{{ old('quantity') }}" id="quantity" class="form-control">
                                            
                                            @error('quantity')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="volume">Volume</label>
                                            <select name="volume" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="MT">Metric Tonne</option>
                                                <option value="kG">Kilogram</option>
                                                <option value="LITRE">Litres</option>
                                            </select>

                                            @error('volume')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="cost_of_funds">Cost of Funds / Landing Cost</label>
                                            <input type="text" data-type="number" name="cost_of_funds" id="cost_of_funds" disabled class="form-control" value="{{ old('cost_of_funds') }}">
                                            
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="selling_price">Selling Price</label>
                                            <input type="text" data-type="number" name="selling_price" value="{{ old('selling_price') }}" id="selling_price" class="form-control" onkeyup="calculateLandingCost()">
                                            
                                            @error('selling_price')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3" align="center">
                                        <div class="col-lg-12">
                                            <button
                                                type="button" 
                                                data-toggle="modal" 
                                                onclick="showInfo()" 
                                                id="analyze_transaction" 
                                                data-target="#profitLossModal"
                                                disabled="disabled"
                                                class="btn btn-primary">
                                                    Analyze Trsansaction
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="profitLossModal" class="modal fade mt-0" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title w-100 text-center" id="lt-sp-1">
                        Transaction Analysis for <span id="product_name_value"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive mt-0">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Transaction Details</th>
                                <th>Info / Expenses</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                                <tr>
                                    <td>Price Per KG</td>
                                    <td>&#8358;<span id="price_per_kg_value"></span></td>
                                </tr>
                                <tr>
                                    <td>Loading Expenses</td>
                                    <td>&#8358;<span id="loading_expenses_value"></span></td>
                                </tr>
                                <tr>
                                    <td>Paper Expenses</td>
                                    <td>&#8358;<span id="paper_expenses_value"></span></td>
                                </tr>
                                <tr>
                                    <td>Transportation Cost</td>
                                    <td>&#8358;<span id="transportation_cost_value"></span></td>
                                </tr>
                                <tr>
                                    <td>Tonnage</td>
                                    <td>&#8358;<span id="tonnage_value"></span></td>
                                </tr>
                                <tr style="border-bottom: 1px solid #eee;">
                                    <td>
                                        <h5><b>Cost of Funds</b></h5>
                                        &#8358;<span id="cost_of_funds_value"></span>
                                    </td>
                                    <td>
                                        <h5><b>Selling Price</b></h5>
                                        &#8358;<span id="selling_price_value"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p class="text-center mt-3 font-weight-bold">
                            <span id="display_message_value"></span>
                        </p>
                        
                        <p class="text-center">
                            <button class="btn btn-primary btn-sm" id="proceed_btn" onclick="transactionFormSubmit()">Proceed</button> 
                            <button class="btn btn-light btn-sm" data-dismiss="modal">Cancel</button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ url('/js/local-transaction.js') }}"></script>
@endsection