@extends('layouts.agro-commodity-template')

@section('content-wrapper')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card position-relative">
                    <div class="card-body">
                        <div class="row" id="new-transaction-row">
                            <div class="col-lg-6">
                                <h3 class="font-weight-bold text-gray" id="lt-sp-2">DEAL MEMO</h3>
                            </div>
                            <div class="col-lg-6 text-right">
                                <img src="https://tradersofafrica.com/main/img/logo.png" style="max-width: 25%"
                                    class="img-fluid" />
                            </div>
                        </div>

                        <div class="row mt-5 pb-2 border-bottom">
                            <div class="col-lg-6">
                                <h5><b>Buyer Details</b></h5>
                                <p>{{ $transaction->buyer_name }}</p>
                                <p>{{ $transaction->buyer_phone }}</p>
                                <p>{{ $transaction->delivery_location }}</p>
                            </div>
                            <div class="col-lg-6">
                                <h5><b>Supplier Details</b></h5>
                                <p>{{ $transaction->supplier_name }}</p>
                                <p>{{ $transaction->supplier_phone }}</p>
                                <p>{{ $transaction->pickup_location }}</p>
                            </div>
                        </div>
                        <div class="row pb-2 mt-3">
                            <div class="col-lg-6">
                                <p>Ref Number: {{ $transactionCode }}</p>
                                <p>Date: {{ date('M d, Y', strtotime($transaction->created_at)) }}</p>
                                <p>EDD:
                                    {{ $transaction->expected_close_date == 'no_specific_date' ? '----' : $transaction->expected_close_date }}
                                </p>
                                <p>Truck No: {{ $transaction->truck_plate_no }}</p>
                                <p>Driver Info: {{ $transaction->truck_driver_name }} | {{ $transaction->truck_driver_no }}
                                </p>
                            </div>
                            <div class="col-lg-6">
                                @if (\Session::has('success'))
                                    <div class="alert alert-success">
                                        <p class="font-weight-bold">{!! \Session::get('success') !!} </p>
                                    </div>
                                @endif
                                <p>
                                    @if ($transaction->transaction_status == 'funded')
                                        <b class="">Transaction funded by
                                            {{ getUserInfo($transaction->funded_by, 'name') }}</b>
                                    @else
                                        <a href="/fund-transaction/{{ $transactionCode }}"
                                            class="btn btn-warning btn-sm text-white" id="accent-color">Fund this
                                            Transaction <i class="ti-credit-card"></i></a>
                                    @endif

                                    <br /> <br />
                                    <a href="/generate-deal-memo-excel/{{ $transactionCode }}"
                                        class="btn btn-primary btn-sm text-white">Generate Excel Document <i
                                            class="ti-file"></i></a>


                                </p>
                            </div>
                        </div>
                        <div class="row pb-2 mt-3">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-stripped">
                                        <thead class="bg-primary text-white p-0">
                                            <tr class="p-1">
                                                <th>Description</th>
                                                <th>Qty</th>
                                                <th>Unit Price</th>
                                                <th>Amount</th>
                                                <th>Within 2 WEEK(S) @2%</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Trucking / Transportation</td>
                                                <td></td>
                                                <td></td>
                                                <td>&#8358;{{ number_format($transaction->transportation_cost) }}</td>
                                                <td>&#8358;{{ number_format(0.02 * $transaction->transportation_cost + $transaction->transportation_cost) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>QC Test</td>
                                                <td></td>
                                                <td></td>
                                                <td>&#8358;{{ number_format($transaction->quality_check) }}</td>
                                                <td>&#8358;{{ number_format(0.02 * $transaction->quality_check + $transaction->quality_check) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Accomodation Cost</td>
                                                <td></td>
                                                <td></td>
                                                <td>&#8358;{{ number_format($transaction->accommodation_cost) }}</td>
                                                <td>&#8358;{{ number_format(0.02 * $transaction->accommodation_cost + $transaction->accommodation_cost) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Loading Expenses</td>
                                                <td></td>
                                                <td></td>
                                                <td>&#8358;{{ number_format($transaction->loading_expenses) }}</td>
                                                <td>&#8358;{{ number_format(0.02 * $transaction->loading_expenses + $transaction->loading_expenses) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Paper Expenses</td>
                                                <td></td>
                                                <td></td>
                                                <td>&#8358;{{ number_format($transaction->paper_expenses) }}</td>
                                                <td>&#8358;{{ number_format(0.02 * $transaction->paper_expenses + $transaction->paper_expenses) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tonnage</td>
                                                <td></td>
                                                <td></td>
                                                <td>&#8358;{{ number_format($transaction->tonnage) }}</td>
                                                <td>&#8358;{{ number_format(0.02 * $transaction->tonnage + $transaction->tonnage) }}
                                                </td>
                                            </tr>
                                            <tr class="font-weight-bold">
                                                <td>{{ $transaction->product_name }}</td>
                                                <td>{{ $transaction->quantity }}</td>
                                                <td>&#8358;{{ $transaction->selling_price }} <small>(Selling Price)</small>
                                                </td>
                                                <td>&#8358;{{ number_format($transaction->selling_price * $transaction->quantity) }}
                                                </td>
                                                <td>
                                                    &#8358;{{ number_format(0.02 * ($transaction->selling_price * $transaction->quantity) + $transaction->selling_price * $transaction->quantity) }}
                                                </td>
                                            </tr>
                                            <tr class="font-weight-bold">
                                                <td>Less (COS)</td>
                                                <td>{{ $transaction->quantity }}</td>
                                                <td>&#8358;{{ $transaction->cost_of_funds }} <small>(Landing Cost)</small>
                                                </td>
                                                <td>&#8358;{{ $transaction->cost_of_funds * $transaction->quantity }}
                                                </td>
                                                <td>
                                                    &#8358;{{ 0.02 * ($transaction->cost_of_funds * $transaction->quantity) + $transaction->cost_of_funds * $transaction->quantity }}
                                                </td>
                                            </tr>
                                            <tr class="font-weight-bold">
                                                <td></td>
                                                <td>
                                                    @if ($transaction->profit == 1)
                                                        <b class="text-success">Profit</b>
                                                    @else
                                                        <b class="text-danger">Loss</b>
                                                    @endif
                                                </td>
                                                <td>&#8358;{{ $transaction->selling_price - $transaction->cost_of_funds }}
                                                </td>
                                                <td>&#8358;{{ ($transaction->selling_price - $transaction->cost_of_funds) * $transaction->quantity }}
                                                </td>
                                                <td>&#8358;{{ 0.02 * (($transaction->selling_price - $transaction->cost_of_funds) * $transaction->quantity) + ($transaction->selling_price - $transaction->cost_of_funds) * $transaction->quantity }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
