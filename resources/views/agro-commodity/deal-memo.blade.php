<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>
    <div class="container">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card position-relative">
                        <div class="card-body">
                            <div class="row" id="new-transaction-row">
                                <table>
                                    <tr>
                                        <td style=""></td>
                                        <td style=" background-color: #DD4F05;"></td>
                                        <td style="background-color: #DD4F05;"></td>
                                        <td style="background-color: #DD4F05;"></td>
                                        <td style="background-color: #DD4F05;"></td>
                                        <td style="background-color: #DD4F05;"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="font-weight: bold; width: 33px; height: 25px;">
                                            <h3>DEAL MEMO</h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="row mt-5 pb-2 border-bottom">
                                <table>
                                    <tr>
                                        <td style="border-style: none;"></td>
                                        <td>
                                            <h5><b>Buyer Details</b></h5>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="width: 30px; border:none;">
                                            <h5><b>Supplier Details</b></h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <p>{{ $transaction->buyer_name }}</p>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <p>{{ $transaction->supplier_name }}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <p>{{ $transaction->buyer_phone }}</p>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <p>{{ $transaction->supplier_phone }}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <p>{{ $transaction->delivery_location }}</p>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <p>{{ $transaction->pickup_location }}</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="row pb-2 mt-3">
                                <div class="col-lg-6">
                                    <table>
                                        <tr>
                                            <td></td>
                                            <td style="width: 25px; border:none;"><b>Extra Information</b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Ref Number: {{ $transactionCode }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Date: {{ date('M d, Y', strtotime($transaction->created_at)) }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>EDD:
                                                {{ $transaction->expected_close_date == 'no_specific_date' ? '----' : $transaction->expected_close_date }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Truck No: {{ $transaction->truck_plate_no }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Driver Info: {{ $transaction->truck_driver_name }} |
                                                {{ $transaction->truck_driver_no }}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row pb-2 mt-3">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-stripped">
                                            <thead class="bg-primary text-white p-0">
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                                <tr class="p-1">
                                                    <th></th>
                                                    <th
                                                        style="vertical-align: middle; text-align: center; height: 25px; background-color: #DD4F05; color: white;">
                                                        <b>Description</b>
                                                    </th>
                                                    <th
                                                        style="background-color: #DD4F05; text-align: center; color: white;">
                                                        <b>Qty</b>
                                                    </th>
                                                    <th style="width: 25px; background-color: #DD4F05; color: white">
                                                        <b>Unit Price</b>
                                                    </th>
                                                    <th
                                                        style="width: 20px; text-align: center; background-color: #DD4F05; color: white;">
                                                        <b>Amount</b>
                                                    </th>
                                                    <th
                                                        style="background-color: #DD4F05; text-align: center; color: white;">
                                                        <b>Within 2 WEEK(S) @2%</b>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td style="">Trucking / Transportation</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $transaction->transportation_cost }}
                                                    </td>
                                                    <td>{{ 0.02 * $transaction->transportation_cost + $transaction->transportation_cost }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>QC Test</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $transaction->quality_check }}</td>
                                                    <td>{{ 0.02 * $transaction->quality_check + $transaction->quality_check }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>Accomodation Cost</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $transaction->accommodation_cost }}
                                                    </td>
                                                    <td>{{ 0.02 * $transaction->accommodation_cost + $transaction->accommodation_cost }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>Loading Expenses</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $transaction->loading_expenses }}</td>
                                                    <td>{{ 0.02 * $transaction->loading_expenses + $transaction->loading_expenses }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>Paper Expenses</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $transaction->paper_expenses }}</td>
                                                    <td>{{ 0.02 * $transaction->paper_expenses + $transaction->paper_expenses }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>Tonnage</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $transaction->tonnage }}</td>
                                                    <td>{{ 0.02 * $transaction->tonnage + $transaction->tonnage }}
                                                    </td>
                                                </tr>
                                                <tr class="font-weight-bold">
                                                    <td></td>
                                                    <td>{{ $transaction->product_name }}</td>
                                                    <td>{{ $transaction->quantity }}</td>
                                                    <td>{{ $transaction->selling_price }} <small>(Selling
                                                            Price)</small>
                                                    </td>
                                                    <td>{{ $transaction->selling_price * $transaction->quantity }}
                                                    </td>
                                                    <td>
                                                        {{ 0.02 * ($transaction->selling_price * $transaction->quantity) + $transaction->selling_price * $transaction->quantity }}
                                                    </td>
                                                </tr>
                                                <tr class="font-weight-bold">
                                                    <td></td>
                                                    <td>Less (COS)</td>
                                                    <td>{{ $transaction->quantity }}</td>
                                                    <td>{{ $transaction->cost_of_funds }} <small>(Landing
                                                            Cost)</small>
                                                    </td>
                                                    <td>{{ $transaction->cost_of_funds * $transaction->quantity }}
                                                    </td>
                                                    <td>
                                                        {{ 0.02 * ($transaction->cost_of_funds * $transaction->quantity) + $transaction->cost_of_funds * $transaction->quantity }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr class="font-weight-bold">
                                                    <td></td>
                                                    <td style="background-color: #28A745; color: white;"></td>
                                                    <td style="background-color: #28A745; color: white;">
                                                        @if ($transaction->profit == 1)
                                                            <b class="text-success">Profit</b>
                                                        @else
                                                            <b class="text-danger">Loss</b>
                                                        @endif
                                                    </td>
                                                    <td style="background-color: #28A745; color: white;">
                                                        {{ $transaction->selling_price - $transaction->cost_of_funds }}
                                                    </td>
                                                    <td style="background-color: #28A745; color: white;">
                                                        {{ ($transaction->selling_price - $transaction->cost_of_funds) * $transaction->quantity }}
                                                    </td>
                                                    <td style="background-color: #28A745; color: white;">
                                                        {{ 0.02 * (($transaction->selling_price - $transaction->cost_of_funds) * $transaction->quantity) + ($transaction->selling_price - $transaction->cost_of_funds) * $transaction->quantity }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
