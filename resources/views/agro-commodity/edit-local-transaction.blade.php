@extends('layouts.agro-commodity-template')

@section('content-wrapper')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card position-relative">
                    <div class="card-body">
                        <div class="row" id="new-transaction-row">
                            <div class="col-md-8 col-lg-8 pl-3 pt-3 pr-3 pb-0 border-right">
                                <h4 id="lt-sp-1" class="font-weight-bold">
                                    Buyer / Seller Info
                                </h4>
                                <hr />
                                <form method="POST" action="{{ url('update-local-transaction/' . $transactionCode) }}">
                                    @csrf
                                    @if (\Session::has('error'))
                                        <div class="alert alert-danger">
                                            <p>{!! \Session::get('error') !!}</p>
                                        </div>
                                    @endif

                                    @if (\Session::has('success'))
                                        <div class="alert alert-success">
                                            <p>{!! \Session::get('success') !!} <a
                                                    href="{{ url('/completed-local-transaction/', $transactionCode) }}">View
                                                    Transaction Memo</a></p>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <input type="hidden" name="transaction_code" value="{{ $transactionCode }}">
                                        <div class="col-lg-6">
                                            <label for="">PickUp Location</label>
                                            <select class="form-control" name="pickup_location">
                                                @if ($transaction->pickup_location != null)
                                                    <option>{{ $transaction->pickup_location }}</option>
                                                @endif
                                                <option value="">- Select -</option>
                                                <option value="Abuja FCT">Abuja FCT</option>
                                                <option value="Abia">Abia</option>
                                                <option value="Adamawa">Adamawa</option>
                                                <option value="Akwa Ibom">Akwa Ibom</option>
                                                <option value="Anambra">Anambra</option>
                                                <option value="Bauchi">Bauchi</option>
                                                <option value="Bayelsa">Bayelsa</option>
                                                <option value="Benue">Benue</option>
                                                <option value="Borno">Borno</option>
                                                <option value="Cross River">Cross River</option>
                                                <option value="Delta">Delta</option>
                                                <option value="Ebonyi">Ebonyi</option>
                                                <option value="Edo">Edo</option>
                                                <option value="Ekiti">Ekiti</option>
                                                <option value="Enugu">Enugu</option>
                                                <option value="Gombe">Gombe</option>
                                                <option value="Imo">Imo</option>
                                                <option value="Jigawa">Jigawa</option>
                                                <option value="Kaduna">Kaduna</option>
                                                <option value="Kano">Kano</option>
                                                <option value="Katsina">Katsina</option>
                                                <option value="Kebbi">Kebbi</option>
                                                <option value="Kogi">Kogi</option>
                                                <option value="Kwara">Kwara</option>
                                                <option value="Lagos">Lagos</option>
                                                <option value="Nassarawa">Nassarawa</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Ogun">Ogun</option>
                                                <option value="Ondo">Ondo</option>
                                                <option value="Osun">Osun</option>
                                                <option value="Oyo">Oyo</option>
                                                <option value="Plateau">Plateau</option>
                                                <option value="Rivers">Rivers</option>
                                                <option value="Sokoto">Sokoto</option>
                                                <option value="Taraba">Taraba</option>
                                                <option value="Yobe">Yobe</option>
                                                <option value="Zamfara">Zamfara</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">Delivery Location</label>
                                            <select class="form-control" name="delivery_location">
                                                @if ($transaction->delivery_location != null)
                                                    <option>{{ $transaction->delivery_location }}</option>
                                                @endif
                                                <option value="">- Select -</option>
                                                <option value="Abuja FCT">Abuja FCT</option>
                                                <option value="Abia">Abia</option>
                                                <option value="Adamawa">Adamawa</option>
                                                <option value="Akwa Ibom">Akwa Ibom</option>
                                                <option value="Anambra">Anambra</option>
                                                <option value="Bauchi">Bauchi</option>
                                                <option value="Bayelsa">Bayelsa</option>
                                                <option value="Benue">Benue</option>
                                                <option value="Borno">Borno</option>
                                                <option value="Cross River">Cross River</option>
                                                <option value="Delta">Delta</option>
                                                <option value="Ebonyi">Ebonyi</option>
                                                <option value="Edo">Edo</option>
                                                <option value="Ekiti">Ekiti</option>
                                                <option value="Enugu">Enugu</option>
                                                <option value="Gombe">Gombe</option>
                                                <option value="Imo">Imo</option>
                                                <option value="Jigawa">Jigawa</option>
                                                <option value="Kaduna">Kaduna</option>
                                                <option value="Kano">Kano</option>
                                                <option value="Katsina">Katsina</option>
                                                <option value="Kebbi">Kebbi</option>
                                                <option value="Kogi">Kogi</option>
                                                <option value="Kwara">Kwara</option>
                                                <option value="Lagos">Lagos</option>
                                                <option value="Nassarawa">Nassarawa</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Ogun">Ogun</option>
                                                <option value="Ondo">Ondo</option>
                                                <option value="Osun">Osun</option>
                                                <option value="Oyo">Oyo</option>
                                                <option value="Plateau">Plateau</option>
                                                <option value="Rivers">Rivers</option>
                                                <option value="Sokoto">Sokoto</option>
                                                <option value="Taraba">Taraba</option>
                                                <option value="Yobe">Yobe</option>
                                                <option value="Zamfara">Zamfara</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="">Supplier Name</label>
                                            <input type="text" name="supplier_name" class="form-control"
                                                value="{{ $transaction->supplier_name }}" />

                                            @error('supplier_name')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="">Buyer Name</label>
                                            <input type="text" name="buyer_name" class="form-control"
                                                value="{{ $transaction->buyer_name }}" />

                                            @error('buyer_name')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="">Supplier Phone</label>
                                            <input type="text" name="supplier_phone" class="form-control"
                                                value="{{ $transaction->supplier_phone }}" />

                                            @error('supplier_phone')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="">Buyer Phone</label>
                                            <input type="text" name="buyer_phone" class="form-control"
                                                value="{{ $transaction->buyer_phone }}" />

                                            @error('buyer_phone')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="">Truck Driver Name</label>
                                            <input type="text" name="truck_driver_name" class="form-control"
                                                value="{{ $transaction->truck_driver_name }}" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">Truck Driver No</label>
                                            <input type="text" name="truck_driver_no" class="form-control"
                                                value="{{ $transaction->truck_driver_no }}" />
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="">Truck Plate Number</label>
                                            <input type="text" name="truck_plate_no" class="form-control"
                                                value="{{ $transaction->truck_plate_no }}" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">Expected close date for this transaction?</label>
                                            <select class="form-control" id="expected_close_date"
                                                name="expected_close_date">
                                                @if ($transaction->expected_close_date == 'no_specific_date')
                                                    <option value="no_specific_date" selected>No Specific Date</option>
                                                    <option value="select_close_date">Select Specific Date</option>
                                                @endif

                                                @if ($transaction->expected_close_date != 'no_specific_date')
                                                    <option value="no_specific_date">No Specific Date</option>
                                                    <option value="select_close_date" selected>Select Specific Date</option>
                                                @endif
                                            </select>
                                        </div>

                                        <div id="select_close_date" class="col-lg-12 mt-3 expected_close_date_selected"
                                            name="expected_close_date_selected"
                                            {{ $transaction->expected_close_date == 'no_specific_date' ? 'style="display:none;"' : '' }}>
                                            <label>Select Date</label>
                                            <input type="date" name="expected_close_date_selected"
                                                value="{{ $transaction->expected_close_date }}" class="form-control"
                                                id="" />
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-lg-12 text-center">
                                            <button type="submit" class="btn btn-primary w-50">Update Information</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4 col-lg-4 pl-3 pt-3 pr-3 pb-0">
                                <h4 id="lt-sp-1" class="font-weight-bold">
                                    Transaction Info
                                </h4>
                                <hr />
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5><b>Product Name</b></h5>
                                        <p>{{ $transaction->product_name }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Price Per KG</b></h5>
                                        <p>{{ number_format($transaction->price_per_kg) }}</p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Quantity</b></h5>
                                        <p>{{ number_format($transaction->quantity) }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Volume</b></h5>
                                        <p>{{ $transaction->volume }}</p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Hotel Cost</b></h5>
                                        <p>&#8358;{{ number_format($transaction->accommodation_cost) }}</p>
                                    </div>

                                    <div class="col-md-6">
                                        <h5><b>Quality Check</b></h5>

                                        @if ($transaction->quality_check != null)
                                            <p>&#8358;{{ number_format($transaction->quality_check) }}</p>
                                        @else
                                            -
                                        @endif
                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Loading Expenses</b></h5>
                                        <p>&#8358;{{ number_format($transaction->loading_expenses) }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Paper Expenses</b></h5>
                                        <p>&#8358;{{ number_format($transaction->paper_expenses) }}</p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Transportation Cost</b></h5>
                                        <p>&#8358;{{ number_format($transaction->transportation_cost) }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Tonnage</b></h5>
                                        <p>&#8358;{{ number_format($transaction->tonnage) }}</p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Cost of Funds</b></h5>
                                        <p>&#8358;{{ number_format($transaction->cost_of_funds) }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Selling Price</b></h5>
                                        <p>&#8358;{{ number_format($transaction->selling_price) }}</p>
                                    </div>
                                </div>
                                <hr />
                                <div class="row mt-3">
                                    <div
                                        class="col-md-12 text-center font-weight-bold {{ $transaction->profit == true ? 'text-success' : 'text-danger' }} ">
                                        {{ $transaction->profit == true ? 'Profit' : 'Loss' }}
                                        <h4>&#8358;
                                            {{ $transaction->selling_price - $transaction->cost_of_funds }}
                                        </h4>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(function() {
            $('#expected_close_date').change(function() {
                $('.expected_close_date_selected').hide();
                $('#' + $(this).val()).show();
            });
        });

    </script>
@endsection
