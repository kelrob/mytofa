@extends('layouts.agro-commodity-template')

@section('content-wrapper')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card position-relative">
                    <div class="card-body">
                        <div class="row" id="new-transaction-row">
                            <div class="col-md-6">
                                <a class="btn btn-primary btn-sm text-white" href="#" onclick="selectTransactionType()">
                                    Start a new Transaction <i class="ti-shopping-cart-full"></i>
                                </a>
                            </div>
                            <div class="col-md-6 float-right">
                                <form action="{{ url('/search/my-transaction') }}" method="GET">
                                    <div class="input-group md-form form-sm form-2 pl-0">
                                        <input class="form-control my-0 py-1 red-border" type="text"
                                            placeholder="Search Transaction Code" name="search" aria-label="Search">
                                        <div class="input-group-append">
                                            <button type="submit" class="p-0 border-0">
                                                <span class="input-group-text red lighten-3" id="basic-text1"><i
                                                        class="ti ti-search text-grey" aria-hidden="true"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-12 p-3">
                                <div class="table-responsive mt-5">
                                    <table class="table table-hover mb-2">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Ref No</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Date Created</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($myTransactions as $transaction)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $transaction->transaction_code }}</td>
                                                    <td>
                                                        {{ $transaction->quantity . ' ' . $transaction->volume . ' of ' . $transaction->product_name }}
                                                    </td>
                                                    <td>
                                                        @if ($transaction->transaction_status == null)
                                                            <small>
                                                                <span class="bg-danger p-1 text-white rounded">
                                                                    Not funded
                                                                </span>
                                                            </small>
                                                        @else
                                                            <small>
                                                                <span class="bg-success p-1 text-white rounded">
                                                                    Funded
                                                                </span>
                                                            </small>
                                                        @endif
                                                    </td>
                                                    <td>{{ date('M d, Y', strtotime($transaction->created_at)) }}</td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <button class="btn btn-light btn-sm" type="button"
                                                                id="actionMenu" data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                                <i class="ti-more"></i>
                                                            </button>
                                                            @if ($transaction->transaction_type == 'local')
                                                                <div class="dropdown-menu shadow bg-white rounded"
                                                                    id="more-action" aria-labelledby="actionMenu">
                                                                    <a class="dropdown-item"
                                                                        href={{ url('completed-local-transaction/' . $transaction->transaction_code) }}>
                                                                        View Transaction
                                                                    </a>
                                                                    <a class="dropdown-item"
                                                                        href={{ url('edit-local-transaction/' . $transaction->transaction_code) }}>
                                                                        Edit Tranaction Info
                                                                    </a>
                                                                </div>
                                                            @elseif($transaction->transaction_type ==
                                                                'international')
                                                                <div class="dropdown-menu shadow bg-white rounded"
                                                                    id="more-action" aria-labelledby="actionMenu">
                                                                    <a class="dropdown-item"
                                                                        href={{ url('completed-international-transaction/' . $transaction->transaction_code) }}>
                                                                        View Transaction
                                                                    </a>
                                                                    <a class="dropdown-item"
                                                                        href={{ url('edit-international-transaction/' . $transaction->transaction_code) }}>
                                                                        Edit Tranaction Info
                                                                    </a>
                                                                </div>
                                                            @endif

                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="display: none;" id="select-transaction-row">
                            <div class="col-md-6 p-3 border-right-sm">
                                <div class="text-center">
                                    <h1 class="text-primary"><i class="ti-truck"></i></h1>
                                    <h4 class="font-weight-bold" id="lt-sp-1">Local Transaction</h4>
                                    <a class="btn btn-primary text-white" href="{{ url('start-local-transaction') }}">
                                        Start Local Transaction <i class="ti-shopping-cart-full"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 p-3">
                                <div class="text-center">
                                    <h1 class="text-primary"><i class="ti-world"></i></h1>
                                    <h4 class="font-weight-bold" id="lt-sp-1">International Transaction</h4>
                                    <a class="btn btn-primary text-white" href="#">
                                        Start International Transaction <i class="ti-shopping-cart-full"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        const selectTransactionType = () => {
            $('#new-transaction-row').fadeOut();
            $('#select-transaction-row').fadeIn('slow').delay(2000);
        }

    </script>
@endsection
