@extends('layouts.agro-commodity-template')

@section('content-wrapper')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card position-relative">
                    <div class="card-body">
                        <div class="row" id="new-transaction-row">
                            <div class="col-md-8 col-lg-8 pl-3 pt-3 pr-3 pb-0 border-right">
                                <h4 id="lt-sp-1" class="font-weight-bold">
                                    Buyer / Seller Info
                                </h4>
                                <hr />
                                <form method="POST" action="{{ url('process-local-transaction') }}">
                                    @csrf
                                    @if (\Session::has('error'))
                                        <div class="alert alert-danger">
                                            <p>{!! \Session::get('error') !!}</p>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <input type="hidden" name="transaction_code" value="{{ $transactionCode }}">
                                        <div class="col-lg-6">
                                            <label for="">PickUp Location</label>
                                            <select class="form-control" name="pickup_location">
                                                @include('includes.states')
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">Delivery Location</label>
                                            <select class="form-control" name="delivery_location">
                                                @include('includes.states')
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="">Supplier Name</label>
                                            <input type="text" name="supplier_name" class="form-control" value="{{ old('supplier_name') }}" />

                                            @error('supplier_name')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="">Buyer Name</label>
                                            <input type="text" name="buyer_name" class="form-control" value="{{ old('buyer_name') }}" />

                                            @error('buyer_name')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="">Supplier Phone</label>
                                            <input type="text" name="supplier_phone" class="form-control" value="{{ old('supplier_phone') }}" />

                                            @error('supplier_phone')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="">Buyer Phone</label>
                                            <input type="text" name="buyer_phone" class="form-control" value="{{ old('buyer_phone') }}" />

                                            @error('buyer_phone')
                                                <small class="" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="">Truck Driver Name</label>
                                            <input type="text" name="truck_driver_name" class="form-control" value="{{ old('truck_driver_name') }}" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">Truck Driver No</label>
                                            <input type="text" name="truck_driver_no" class="form-control" value="{{ old('truck_driver_no') }}" />
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-6">
                                            <label for="">Truck Plate Number</label>
                                            <input type="text" name="truck_plate_no" class="form-control" value="{{ old('truck_plate_no') }}" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">Expected close date for this transaction?</label>
                                            <select class="form-control" id="expected_close_date" name="expected_close_date">
                                                <option value="no_specific_date">No Specific Date</option>
                                                <option value="select_close_date">Select Specific Date</option>
                                            </select>
                                        </div>
                                        <div id="select_close_date" class="col-lg-12 mt-3 expected_close_date_selected" name="expected_close_date_selected" style="display:none;">
                                            <label>Select Date</label>
                                            <input type="date" name="expected_close_date_selected" value="{{ old('expected_close_date_selected') }}" class="form-control" id="">
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-lg-12 text-center">
                                            <button type="submit" class="btn btn-primary w-50">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4 col-lg-4 pl-3 pt-3 pr-3 pb-0">
                                <h4 id="lt-sp-1" class="font-weight-bold">
                                    Transaction  Info
                                </h4>
                                <hr />
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5><b>Product Name</b></h5>
                                        <p>{{ $transaction->product_name }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Price Per KG</b></h5>
                                        <p>{{ number_format($transaction->price_per_kg) }}</p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Quantity</b></h5>
                                        <p>{{ number_format($transaction->quantity) }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Volume</b></h5>
                                        <p>{{ $transaction->volume }}</p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Hotel Cost</b></h5>
                                        <p>&#8358;{{ number_format($transaction->accommodation_cost) }}</p>
                                    </div>
                                   
                                    <div class="col-md-6">
                                        <h5><b>Quality Check</b></h5>
                                       
                                        @if ($transaction->quality_check != null)
                                            <p>&#8358;{{ number_format($transaction->quality_check) }}</p>
                                        @else
                                            -
                                        @endif
                                    </div>
                                    
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Loading Expenses</b></h5>
                                        <p>&#8358;{{ number_format($transaction->loading_expenses) }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Paper Expenses</b></h5>
                                        <p>&#8358;{{ number_format($transaction->paper_expenses) }}</p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Transportation Cost</b></h5>
                                        <p>&#8358;{{ number_format($transaction->transportation_cost) }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Tonnage</b></h5>
                                        <p>&#8358;{{ number_format($transaction->tonnage) }}</p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <h5><b>Cost of Funds</b></h5>
                                        <p>&#8358;{{ number_format($transaction->cost_of_funds) }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Selling Price</b></h5>
                                        <p>&#8358;{{ number_format($transaction->selling_price) }}</p>
                                    </div>
                                </div>
                                <hr />
                                <div class="row mt-3">
                                    <div class="col-md-12 text-center font-weight-bold {{ $transaction->profit == true ? 'text-success' : 'text-danger' }} ">
                                        {{ $transaction->profit == true ? 'Profit' : 'Loss' }}
                                        <h4>&#8358;
                                            {{ $transaction->selling_price - $transaction->cost_of_funds }}
                                        </h4>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-md-12 text-center">
                                        <button onclick="deleteTransactionWarning({{ $transactionCode }})" class="btn btn-danger text-white">Cancel Transaction</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#expected_close_date').change(function () {
                $('.expected_close_date_selected').hide();
                $('#' + $(this).val()).show();
            });
        });


        const deleteTransactionWarning = (transactionCode) => {
            if (confirm('Are you sure you want to cancel this transaction?')) {
                window.location.href = `../cancel-local-transaction/${transactionCode}`;
            }
        }
    </script>

@endsection