@extends('layouts.agro-commodity-template')

@section('content-wrapper')
    <div class="content-wrapper">

        <div class="row bg-white p-1">
            <div class="col-md-4 border-right grid-margin stretch-card">
                <div class="">
                    <div class="card-body text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <img src="{{ url('images/dashboard/avatar.png') }}" style="max-width: 45%;" alt="profile"
                                    class="img-fluid" />
                                <h4 class="mt-4 font-weight-bold" id="lt-sp-1">{{ Auth::user()->name }}</h4>
                                <p class="font-weight-bold" style="color: #DE5711;">{{ Auth::user()->department }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8 grid-margin stretch-card">
                <div class="card-body">
                    <div class="row">
                        <form action={{ url('/update-profile') }} method="POST" id="profile-form">
                            @csrf
                            @if (\Session::has('success'))
                                <div class="alert alert-success">
                                    <p>{!! \Session::get('success') !!} </p>
                                </div>
                            @endif

                            <div class="form-group col-lg-8">
                                <label for="email">Email Address</label>
                                <input type="email" name="email" disabled class="form-control"
                                    value="{{ Auth::user()->email }}">
                            </div>

                            <div class="form-group col-lg-8">
                                <label for="phone">Phone</label>
                                <input type="text" name="phone" class="form-control" value="{{ Auth::user()->phone }}">
                            </div>

                            <div class="form-group col-lg-8">
                                <label for="address">Home Address</label>
                                <input type="text" name="address" class="form-control" value="{{ Auth::user()->address }}">
                            </div>

                            <div class="form-group col-lg-8">
                                <input type="submit" name="submit" class="btn btn-primary w-100">
                                <p class="mt-3"><a href="#" data-toggle="modal" data-target="#changePasswordModal">Change
                                        Password</a></p>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Change Password Modal -->
    <div id="changePasswordModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="" id=""><b>Change Account Password!</b></h4>
                            <form class="mt-4">
                                <div class="alert alert-danger" id="error_message" style="display: none;"></div>
                                <div class="alert alert-success" id="success_message" style="display: none;"></div>
                                @csrf
                                <div class="form-group">
                                    <label for="current_password"><b>Curent Password</b></label>
                                    <input type="password" class="form-control" id="current_password"
                                        name="current_password">
                                </div>

                                <div class="form-group">
                                    <label for="new_password"><b>New Password</b></label>
                                    <input type="password" class="form-control" id="new_password" name="new_password">
                                </div>

                                <div class="form-group">
                                    <label for="new_password_confirm"><b>New Password Again</b></label>
                                    <input type="password" class="form-control" id="new_password_confirm"
                                        name="new_password_confirm">
                                </div>

                                <div class="form-group">
                                    <button type="button" id="change-btn" onclick="changePassword()"
                                        class="btn btn-primary w-100"><span id="change-password-text">Change
                                            Password</span> <i class="ti-lock"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- load jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        const changePassword = () => {

            $('#change-password-text').text('Please wait');
            $('#change-btn').attr('disabled', 'disabled');

            let errorMessage;
            let currentPassword = $('#current_password').val();
            let newPassword = $('#new_password').val();
            let newPasswordConfirm = $('#new_password_confirm').val();

            let data = {
                currentPassword,
                newPassword,
                newPasswordConfirm
            }

            $.ajax({
                type: 'POST',
                url: 'change-password',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('#change-btn').removeAttr('disabled', 'disabled');
                    $('#change-password-text').text('Change Password');

                    if (data.status == false) {
                        $('#success_message').hide();
                        $('#error_message').show().html(`${data.message}`);
                    }

                    if (data.status == true) {
                        $('#error_message').hide();
                        $('#success_message').show().html(`${data.message}`);
                    }
                }
            });
        }

    </script>
@endsection
