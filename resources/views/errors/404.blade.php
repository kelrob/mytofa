
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>My Tofa</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ url('/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ url('/vendors/base/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ url('/css/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" type="image/png" href="{{ url('https://tradersofafrica.com/main/img/fav.png') }}">

    <link href="https://fonts.googleapis.com/css2?family=Khula&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Khula', sans-serif;
        }
    </style>
</head>

<body>
<div class="container-scroller" id="body">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
            <div class="row flex-grow">
                <div class="col-lg-6 d-flex align-items-center justify-content-center bg-white">
                    <div class="auth-form-transparent text-left p-3">
                        <div class="brand-logo">
                            <img src="https://tradersofafrica.com/main/img/logo.png" alt="logo">
                        </div>
                        <h4>OOPS! Page not found!</h4>
                        <h1 class="font-weight-light">404</h1>
                        <a href="{{ url()->previous() }}" class="btn btn-primary">Go Back</a>
                    </div>
                </div>
                <div class="col-lg-6 login-half-bg d-flex flex-row">
                    <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright
                        &copy; {{ date('Y') }} All rights reserved.</p>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{ url('/vendors/base/vendor.bundle.base.js') }}"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="{{ url('/js/off-canvas.js') }}"></script>
<script src="{{ url('/js/hoverable-collapse.js') }}"></script>
<script src="{{ url('/js/template.js') }}"></script>
<script src="{{ url('/js/todolist.js') }}"></script>
<!-- endinject -->
</body>

</html>
