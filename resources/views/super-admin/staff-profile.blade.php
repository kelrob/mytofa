@extends('layouts.super-admin-template')

@section('content-wrapper')
    <div class="content-wrapper">

        <div class="row bg-white p-1">
            <div class="col-md-4 border-right grid-margin stretch-card">
                <div class="">
                    <div class="card-body text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                @if (\Session::has('success'))
                                    <div class="alert alert-success">
                                        <p>{!! \Session::get('success') !!} </p>
                                    </div>
                                @endif
                                <img src="{{ url('images/dashboard/avatar.png') }}" style="max-width: 45%;" alt="profile"
                                    class="img-fluid" />
                                <h4 class="mt-4 font-weight-bold" id="lt-sp-1">{{ $staff->name }}</h4>
                                <p class="font-weight-bold" style="color: #DE5711;">{{ $staff->department }}</p>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-lg-12 text-center">
                                <p>
                                    @if ($staff->disabled == 0)
                                        <a href="{{ url('disable-staff/' . $staff->id . '/1') }}"
                                            class="btn btn-danger btn-sm">Disable
                                            Account</a>
                                    @elseif($staff->disabled == 1)
                                        <a href="{{ url('disable-staff/' . $staff->id . '/0') }}"
                                            class="btn btn-success btn-sm">Activate Account</a>
                                    @endif
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-8 grid-margin stretch-card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 id="lt-sp-1" class="font-weight-bold">Contact Info</h4>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-6">
                            <p>
                                <b>Email</b> <br />
                                {{ $staff->email }}
                            </p>
                        </div>

                        <div class="col-lg-6">
                            <p>
                                <b>Phone</b> <br />
                                {{ $staff->phone == null ? '-' : $staff->phone }}
                            </p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-6">
                            <p>
                                <b>Address</b> <br />
                                {{ $staff->address == null ? '-' : $staff->address }}
                            </p>
                        </div>

                        <div class="col-lg-6">

                        </div>
                    </div>


                    <hr>
                    @if ($staff->department != 'Finance')
                        <div class="row mt-3">
                            <div class="col-lg-4 border-right">
                                <b class="text-warning">Ongoing Transactions</b>
                                <h1 class="mt-3">{{ $ongoingTransaction }}</h1>
                            </div>
                            <div class="col-lg-4 border-right">
                                <b class="text-success">Completed Transactions</b>
                                <br>
                                <h1 class="mt-3">{{ $completedTransaction }}</h1>
                            </div>
                            <div class="col-lg-4">
                                <b class="text-danger">Non Funded Transactions</b>
                                <br>
                                <h1 class="mt-3">{{ $nonFundedTransaction }}</h1>
                            </div>
                        </div>
                    @else
                        <div class="row mt-3">
                            <div class="col-lg-6 border-right">
                                <b class="text-success">Funded Transactions</b>
                                <br>
                                <h1 class="mt-3">{{ $financeFundedTransaction }}</h1>
                            </div>
                            <div class="col-lg-6">
                                <b class="text-danger">Non Funded Transactions</b>
                                <br>
                                <h1 class="mt-3">{{ $nonFundedTransaction }}</h1>
                            </div>
                        </div>
                    @endif


                    @if ($staff->department != 'Finance')
                        <div class="row text-center mt-5">
                            <div class="col-lg-12">
                                <a href="{{ url('staff-transactions') . '/' . $staff->id }}"
                                    class="btn btn-primary btn-sm">View
                                    Transactions</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection
