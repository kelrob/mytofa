@extends('layouts.super-admin-template')
@section('content-wrapper')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card position-relative">
                    <div class="card-body">
                        <div class="row  p-3">

                            @if ($staffCount == 0)
                                <div class="col-lg-12">
                                    <div class="text-center">
                                        <h4 class="font-weight-bold" id="lt-sp-1">No Staff Found</h4>
                                        <a class="btn btn-primary text-white" href="{{ url('add-staff') }}">
                                            Add a New Staff <i class="ti-user"></i>
                                        </a>
                                    </div>
                                </div>
                            @else
                                <div class="col-lg-6">
                                    <div class="btn btn-primary btn-sm">
                                        <a href="{{ url('add-staff') }}" class="text-white">Add a New Staff <i
                                                class="ti-user"></i></a>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <form action="{{ url('/search/staff') }}" method="GET">
                                        <div class="input-group md-form form-sm form-2 pl-0">
                                            <input class="form-control my-0 py-1 red-border" type="text"
                                                placeholder="Search Staff name" name="staff" aria-label="Search">
                                            <div class="input-group-append">
                                                <button type="submit" class="p-0 border-0">
                                                    <span class="input-group-text red lighten-3" id="basic-text1"><i
                                                            class="ti ti-search text-grey" aria-hidden="true"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-12 mt-3">

                                    <div class="table-responsive">
                                        <table class="table table-hover mb-2">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Name</th>
                                                    <th>Department</th>
                                                    <th>Invitation Status</th>
                                                    <th>Account Status</th>
                                                    <th>Total Transaction(s)</th>
                                                    <th>Info</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($staffs as $staff)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $staff->name }}</td>
                                                        <td>{{ $staff->department }}</td>
                                                        <td>
                                                            @if ($staff->invitation_accepted == 0)
                                                                <label class="badge badge-danger">Pending</label>
                                                            @elseif($staff->invitation_accepted == 1)
                                                                <label class="badge badge-success">Accepted</label>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if ($staff->disabled == 1)
                                                                <label class="badge text-danger">Suspended</label>
                                                            @else
                                                                <label class="badge text-success">Active</label>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if ($staff->transaction->count() <= 0)
                                                                -
                                                            @else
                                                                {{ $staff->transaction->count() }}
                                                            @endif
                                                        </td>
                                                        <td><a href="{{ url('staff/' . $staff->id) }}"
                                                                class="text-decoration-none">View Profile</a></td>
                                                        <td>
                                                            <div class="dropdown">
                                                                <button class="btn btn-light btn-sm" type="button"
                                                                    id="actionMenu" data-toggle="dropdown"
                                                                    aria-haspopup="true" aria-expanded="false">
                                                                    <i class="ti-more"></i>
                                                                </button>
                                                                <div class="dropdown-menu shadow bg-white rounded"
                                                                    id="more-action" aria-labelledby="actionMenu">
                                                                    <a class="dropdown-item"
                                                                        href="{{ url('staff/' . $staff->id) }}">View
                                                                        Profile</a>
                                                                    <a class="dropdown-item" href="#">Delete Staff
                                                                        Account</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        {{ $staffs->links() }}
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
