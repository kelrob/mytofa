@extends('layouts.super-admin-template')

@section('content-wrapper')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card position-relative">
                    <div class="card-body">
                        <h4 class="font-weight-bold" id="lt-sp-1">Add a new Staff</h4>
                        <div class="row">
                            <div class="col-md-12 p-3">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (\Session::has('success'))
                                    <div class="alert alert-success">
                                        <p>{!! \Session::get('success') !!}</p>
                                    </div>
                                @endif
                                <form class="forms-sample" method="post" action="{{ url('new-staff') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="fullname">Fullname</label>
                                        <input type="text" class="form-control" value="{{ old('fullname') }}"
                                            name="fullname" id="fullname" placeholder="Fullname" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="email" name="email" value="{{ old('email') }}" class="form-control"
                                            id="email" placeholder="Email" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="department">Department</label>
                                        <select id="department" name="department" class="form-control" required>
                                            <option value="">Please Select</option>
                                            <option value="Agro Commodity">Agro Commodity</option>
                                            <option value="Source Pro">Source Pro</option>
                                            <option value="Field Officer">Field Officer</option>
                                            <option value="Finance">Finance</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary mr-2">Send Invite Request <i
                                            class="ti-bolt-alt"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
