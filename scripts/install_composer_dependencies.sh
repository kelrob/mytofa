#!/bin/bash
cd /var/www/html/directoryName
sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo /sbin/swapon /var/swap.1

sudo composer install
sudo php artisan key:generate
sudo chmod -R 777 storage
# sudo php artisan migrate
# sudo composer dump-autoload
# sudo php artisan db:seed 