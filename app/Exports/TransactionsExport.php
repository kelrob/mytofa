<?php

namespace App\Exports;

use App\Transaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class TransactionsExport implements FromView, WithColumnFormatting
{
    public $transactionCode;

    public function __construct($transactionCode) {
        $this->transactionCode = $transactionCode;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View {
        $transaction = Transaction::where('transaction_code', $this->transactionCode)->first();
        $transactionExist = Transaction::where('transaction_code', $this->transactionCode)->count();

        $transactionCode = $this->transactionCode;
        return view('agro-commodity.deal-memo', compact('transaction', 'transactionCode'));
        
    }

    public function columnFormats(): array {
        return [
            'E' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }
}
