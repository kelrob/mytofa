<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaction;

class PagesController extends Controller
{
    public function index() {

        $fundedTransactionCount = Transaction::whereTransactionStatus('funded')->count();
        $nonFundedTransactionCount = Transaction::whereTransactionStatus(null)->count();

        return view('finance.index', compact('fundedTransactionCount', 'nonFundedTransactionCount'));
    }

    public function fundedTransactions() {

        $transactions = Transaction::with('user')->whereTransactionStatus('funded')->orderBy('created_at', 'DESC')->paginate(10);

        return view('finance.funded_transactions', compact('transactions'));
    }

    public function nonFundedTransactions() {
        $transactions = Transaction::with('user')->whereTransactionStatus(null)->orderBy('created_at', 'DESC')->paginate(10);

        return view('finance.non_funded_transactions', compact('transactions'));
    }
}
