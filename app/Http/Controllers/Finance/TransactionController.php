<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Transaction;
use App\Traits\SendCustomMailTrait;
use App\Traits\SendCustomNotificationTrait;

class TransactionController extends Controller
{

    use SendCustomMailTrait;
    use SendCustomNotificationTrait;

    public function fundTransaction($transactionCode) {
        if (Transaction::whereTransactionCode($transactionCode)->count() == 1) {
            
            $transaction = Transaction::whereTransactionCode($transactionCode)->first();

            // Notifiy Admin and staff that transaction has been funded
            $staff = User::find($transaction->user_id);
            $CEO = User::whereDepartment('CEO')->whereRole('super_admin')->first();

            $this->notifyStaffAndAdminFundedLocalTransaction([$staff->email, $CEO->email], $transactionCode);

            // Send Local Notification
            $localMessage = "Transaction $transactionCode has just been funded";
            $this->sendLocalNotification($staff->id, null, $localMessage, $transactionCode);
            $this->sendLocalNotification($CEO->id, null, $localMessage, $transaction->transaction_code);

            // Upate to DB
            $message = "Transaction $transactionCode has been funded by you";
            $transaction->transaction_status = 'funded';
            $transaction->funded_by = Auth::user()->id;
            $transaction->save();

            return redirect()->back()->with('success', $message);
        }
    }
}
