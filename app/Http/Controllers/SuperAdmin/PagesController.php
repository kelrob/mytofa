<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Transaction;

class PagesController extends Controller
{
    public function staffs() {
        $staffs = User::where('department', '!=', 'CEO')->orderBy('created_at', 'DESC')->paginate(10);
        $staffCount = User::where('department', '!=', 'CEO')->count();
        
        return view('super-admin.staffs', compact('staffCount', 'staffs'));
    }

    public function createStaff() {
        return view('super-admin.create-staff');
    }

    public function acceptInvite($email, $token) {
        try {

            $linkExpired = User::where(['email' => $email, 'invitation_accepted' => 1])->count();

            if ($linkExpired > 0) {
                return Redirect::to(url('404'));
            }

            $decrypted = Crypt::decrypt($token);
            if ($decrypted == $email) {
                if (User::where('email', $email)->count() == 1) {
                    $userFullName = User::where('email', $email)->first()->name;
                    return view('auth.accept-invite', compact('userFullName', 'email'));
                } else {
                    return Redirect::to(url('404'));
                }
            } else {
                return Redirect::to(url('404'));
            }
        } catch (DecryptException $e) {
            return Redirect::to(url('404'));
        }
    }

    public function transactions() {

        $transactions = Transaction::with('user')->orderBy('created_at', 'DESC')->paginate(10);

        return view('super-admin.transactions', compact('transactions'));
    }

    public function viewStaff($id) {

        if (User::whereId($id)->count() == 0) {
            return Redirect::to(url('404'));
        }

        $staff = User::whereId($id)->first();

        $completedTransaction = Transaction::where([
            'user_id' => $id, 
            'transaction_status' => 'completed'
        ])->count();

        $ongoingTransaction = Transaction::where([
            'user_id' => $id, 
            'transaction_status' => 'ongoing'
        ])->count();

        $nonFundedTransaction = Transaction::where([
            'user_id' => $id, 
            'transaction_status' => null
        ])->count();

        $fundedTransaction = Transaction::where([
            'user_id' => $id, 
            'transaction_status' => 'funded'
        ])->count();


        $financeFundedTransaction = Transaction::where([
            'funded_by' => $id, 
            'transaction_status' => 'funded'
        ])->count();

        return view('super-admin.staff-profile', compact('staff','financeFundedTransaction', 'completedTransaction', 'fundedTransaction', 'ongoingTransaction', 'nonFundedTransaction'));
    }

    public function staffTransactions($staffId) {

        $staffName = User::find($staffId)->name;

        $transactions = Transaction::with('user')->whereUserId($staffId)->orderBy('created_at', 'DESC')->paginate(10);
        $transactionCount = Transaction::whereUserId($staffId)->count();
        return view('super-admin.staff-transactions', compact('transactionCount', 'transactions', 'staffName'));
    }
}
