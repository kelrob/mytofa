<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaction;

class TransactionController extends Controller
{
    public function index() {
        return view('index');
    }

    public function searhTransaction(Request $request) {
        $search = $request->search;
        $transactions = Transaction::with('user')->whereTransactionCode($search)->get();

        return view('super-admin.transaction-search-result', compact('transactions', 'search'));
    }
}
