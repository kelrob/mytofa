<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Traits\SendCustomMailTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Transaction;


class StaffController extends Controller
{
    use SendCustomMailTrait;

    public function newStaff(Request $request) {
        $validateData = Validator::make($request->all(), [
           'email' => 'required|email|unique:users',
           'fullname' => 'required',
           'department' => 'required'
        ]);

        if ($validateData->fails()) {
            return Redirect::back()->withInput()->withErrors($validateData->messages());
        } else {
            $user = new User();
            $user->email = $request->email;
            $user->name = $request->fullname;
            $user->department = $request->department;
            $user->role = $request->department == 'Finance' ? 'finance' : 'default';
            $user->save();

            $this->sendInviteMail($user->email);

            return redirect()->back()->with('success', "An Invite has been sent to $request->email");
        }
    }

    public function editProfile(Request $request) {
        $user = Auth::user();

        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->save();

        return redirect()->back()->with('success', "Profile Updated Successfully");
    }

    public function changePassword(Request $request) {
        $user = Auth::user();
        $hashedPassword = $user->password;

        $currentPassword = $request->currentPassword;
        $newPassword = $request->newPassword;
        $newPasswordConfirm = $request->newPasswordConfirm;

        if (empty($currentPassword) || empty($newPassword) || empty($newPasswordConfirm)) {
            return response()->json([
                'status' => false,
                'message' => 'All fields are required'
            ]);
        } else {
            if (Hash::check($currentPassword, $hashedPassword)) {

                if ($newPassword == $newPasswordConfirm) {
                    $newPasswordEncrypted = Hash::make($request->newPassword);
                    $user->password = $newPasswordEncrypted;
                    $user->save();


                    // Notify User
                    $this->sendPasswordChangedMail($user->email);

                    return response()->json([
                        'status' => true,
                        'message' => 'Password changed successfully'
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'New Password and New Password again does not match'
                    ]);
                }


            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Current Password is wrong'
                ]);
            }
        }

        return response()->json($request->all());
    }

    public function disableStaff($staffId, $disabledStatus) {

        User::whereId($staffId)->update(['disabled' => $disabledStatus]);

        $message = $disabledStatus == 1 ? 'Account Disabled Successfully' : 'Account Activated Successfully';
        return redirect()->back()->with('success', $message);
    }

    public function searchStaff(Request $request) {
        $search = $request->staff;
        
        $staffs = User::where('name','LIKE','%'.$search.'%')
        ->where('department', '!=', 'CEO')
                      ->orderBy('created_at', 'DESC')->paginate(10);

        $staffCount = User::where('name','LIKE','%'.$search.'%')
        ->where('department', '!=', 'CEO')
        ->count();

        return view('super-admin.staffs-search-result', compact('staffs', 'staffCount'));
    }
}
