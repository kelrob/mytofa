<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaction;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Total Transaction by user
        $totalTransactionCount = Transaction::all()->count();

        // Completed Total Transaction by user
        $completedTransactionCount = Transaction::whereTransactionStatus('completed')->count();

        // Ongoing Total Transaction by user
        $ongoingTransactionCount = Transaction::whereTransactionStatus('ongoing')->count();

        // Non Funded Transactions
        $nonFundedTransactionCount = Transaction::whereTransactionStatus(null)->count();

        // Funded Transactions
        $fundedTransactionCount = Transaction::whereTransactionStatus('funded')->count();

        return view('super-admin.index', compact('fundedTransactionCount', 'totalTransactionCount', 'completedTransactionCount', 'ongoingTransactionCount', 'nonFundedTransactionCount'));
    }
}
