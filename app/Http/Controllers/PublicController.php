<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PublicController extends Controller
{
    public function completeSignUp(Request $request) {
        $validateData = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validateData->fails()) {
            return Redirect::back()->withInput()->withErrors($validateData->messages());
        } else {
            $user = User::where('email', $request->email)->first();
            $user->password = Hash::make($request->password);
            $user->invitation_accepted = 1;
            $user->save();

            Auth::loginUsingId($user->id);
            return redirect('/home?auth=success');
        }
    }

    public function errorCode401() {
        return view('errors.401');
    }

}
