<?php

namespace App\Http\Controllers\AgroCommodity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\LocalTransactionRequest;
use App\Http\Requests\ProcessLocalTransactionRequest;
use Illuminate\Support\Facades\Auth;
use App\Transaction;
use App\Notification;
use App\User;
use App\Exports\TransactionsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\SendCustomMailTrait;
use App\Traits\SendCustomNotificationTrait;

class TransactionController extends Controller
{

    use SendCustomMailTrait;
    use SendCustomNotificationTrait;

    public function createLocalTransaction(LocalTransactionRequest $request) {

        $finance = User::whereRole('finance')->whereDepartment('Finance')->first();
        $superAdmin = User::whereRole('super_admin')->whereDepartment('CEO')->first();

        // User Info
        $user = Auth::user();
        $staffName = $user->name;

        // Compulsory rules
        $validateData = $request->validated();

        // Remove commas
        $tonnage = str_replace(',', '', $request->tonnage);
        $quantity = str_replace(',', '', $request->quantity);
        $pricePerKG = str_replace(',', '', $request->price_per_kg);
        $productName = str_replace(',', '', $request->product_name);
        $sellingPrice = str_replace(',', '', $request->selling_price);
        $qualityCheck = str_replace(',', '', $request->quality_check);
        $paperExpenses = str_replace(',', '', $request->paper_expenses);
        $loadingExpenses = str_replace(',', '', $request->loading_expenses);
        $accommodationCost = str_replace(',', '', $request->accommodation_cost);
        $transportationCost = str_replace(',', '', $request->transportation_cost);
        

        // Calculate Cost of funds
        $costOfFunds = (($transportationCost + $loadingExpenses + $paperExpenses + $qualityCheck + $accommodationCost) / ($tonnage) + $pricePerKG);
        $costOfFunds = number_format((float)$costOfFunds, 2, '.', '');
        
        // Store to database
        $transaction = new Transaction();
        $transaction->tonnage = $tonnage;
        $transaction->user_id = $user->id;
        $transaction->price_per_kg = $pricePerKG;
        $transaction->product_name = $productName;
        $transaction->quality_check = $qualityCheck;
        $transaction->paper_expenses = $paperExpenses;
        $transaction->loading_expenses = $loadingExpenses;
        $transaction->transportation_cost = $transportationCost;
        $transaction->accommodation_cost = $accommodationCost;
        $transaction->quantity = $quantity;
        $transaction->volume = $request->volume;
        $transaction->transaction_type = 'local';
        $transaction->currency = 'NGN';
        $transaction->selling_price = $sellingPrice;
        $transaction->cost_of_funds = $costOfFunds;
        $transaction->transaction_code = time();
        $sellingPrice > $costOfFunds ? $transaction->profit = true : $transaction->loss = true;
        $transaction->save();

        $message = "New local transaction started by $staffName";


        /**
         * Notify Super Admin and Finance Officer by mail that a new local transaction has started
         * It also needs funding
         */
        $this->notifyAdminStartLocalTransaction($superAdmin->email, $finance->email, $staffName);

        /**
         * Notify Super Admin  and Finance on the dashboard that a new local transaction as started.
         */
        $this->sendLocalNotification($superAdmin->id, null, $message, $transaction->transaction_code);
        $this->sendLocalNotification($finance->id, null, $message . ', and requires your funding', $transaction->transaction_code);


        return redirect('process-local-transaction/' . $transaction->transaction_code);
    }

    public function processLocalTransaction(ProcessLocalTransactionRequest $request) {

        // User Info
        $user = Auth::user();

        // Compulsory rules
        $validateData = $request->validated();

        // Check if user is owner of transaction
        $checkOwnership = Transaction::where('user_id', $user->id)
            ->where('transaction_code', $request->transaction_code)
            ->count();

        // Fetch Transaction
        $transaction = Transaction::where('user_id', $user->id)
            ->where('transaction_code', $request->transaction_code)
            ->first();

        if ($checkOwnership == 1) {

            if ($request->expected_close_date == 'select_close_date' && $request->expected_close_date_selected == null) {
                return redirect()->back()->withInput()->with('error', "Please choose a correct date ");
            }

            $transaction->pickup_location = $request->pickup_location;
            $transaction->delivery_location = $request->delivery_location;
            $transaction->supplier_name = $request->supplier_name;
            $transaction->buyer_name = $request->buyer_name;
            $transaction->truck_driver_name = $request->truck_driver_name;
            $transaction->truck_driver_no = $request->truck_driver_no;
            $transaction->buyer_phone = $request->buyer_phone;
            $transaction->supplier_phone = $request->supplier_phone;
            $transaction->truck_plate_no = $request->truck_plate_no;
            $transaction->expected_close_date = $request->expected_close_date == 'select_close_date' ? $request->expected_close_date_selected : $request->expected_close_date;
            $transaction->save(); 

            return redirect('completed-local-transaction/' . $request->transaction_code);

        } else {
            return redirect()->back()->with('error', "You do not have the permission to perform this action");
        }
    }

    public function cancelLocalTransaction($transactionCode) {
        $user = Auth::user();

        $transaction = Transaction::where('user_id', $user->id)
            ->where('transaction_code', $transactionCode)
            ->first();

        Notification::whereTransactionCode($transactionCode)->first()->delete();
        
        
        $transaction->delete();

        return redirect()->route('my-transactions');
    }


    public function updateLocalTransaction(Request $request, $transactionCode) {
        
        $user = Auth::user();
        $transaction = Transaction::whereTransactionCode($transactionCode)->first();

        if ($user->id == $transaction->user_id) {

            $transaction->pickup_location = $request->pickup_location;
            $transaction->delivery_location = $request->delivery_location;
            $transaction->supplier_name = $request->supplier_name;
            $transaction->buyer_name = $request->buyer_name;
            $transaction->supplier_phone = $request->supplier_phone;
            $transaction->buyer_phone = $request->buyer_phone;
            $transaction->truck_driver_name = $request->truck_driver_name;
            $transaction->truck_driver_no = $request->truck_driver_no;
            $transaction->truck_plate_no = $request->truck_plate_no;
            $transaction->expected_close_date = $request->expected_close_date == 'select_close_date' ? $request->expected_close_date_selected : $request->expected_close_date;
            $transaction->save();

            return redirect()->back()->with('success', "Transaction Updated Successfully");

        } else {
            return redirect()->back()->with('error', "You do not have the permission to perform this action");
        }

    }

    public function dealMemoExcel($transactionCode) {
        $count = Transaction::whereTransactionCode($transactionCode)->count();
        
        if ($count == 1) {
            return Excel::download(new TransactionsExport($transactionCode), 'TR'. $transactionCode . '.xlsx');
        }
    }

    public function searchMyTransactions(Request $request) {
        
        $user = Auth::User();
        $search = $request->search;

        $myTransactions = Transaction::with('user')
            ->whereTransactionCode($search)
            ->whereUserId($user->id)
            ->get();

        return view('agro-commodity.my-transactions-search-result', compact('search', 'myTransactions'));
    }

}
