<?php

namespace App\Http\Controllers\AgroCommodity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Transaction;
use App\Notification;

class PagesController extends Controller
{   
    public function index() {
        $user = Auth::user();

        // Total Transaction by user
        $totalTransactionCount = Transaction::whereUserId($user->id)->count();

        // Completed Total Transaction by user
        $completedTransactionCount = Transaction::where([
            'user_id' => $user->id, 
            'transaction_status' => 'completed'
        ])->count();

        // Ongoing Total Transaction by user
        $ongoingTransactionCount = Transaction::where([
            'user_id' => $user->id, 
            'transaction_status' => 'ongoing'
        ])->count();

        // Non Funded Transactions
        $nonFundedTransactionCount = Transaction::where([
            'user_id' => $user->id, 
            'transaction_status' => null
        ])->count();

        return view('agro-commodity.index', compact('totalTransactionCount', 'completedTransactionCount', 'ongoingTransactionCount', 'nonFundedTransactionCount'));
    }

    public function myTransactions() {

        $user = Auth::user();

        $myTransactions = Transaction::whereUserId($user->id)->orderBy('created_at', 'DESC')->paginate(10);
        $myTransactionsCount = Transaction::where('user_id', $user->id)->count();
        return view('agro-commodity.my-transactions', compact('myTransactionsCount', 'myTransactions'));
    }

    public function createLocalTransaction() {
        return view('agro-commodity.create-local-transaction');
    }

    public function processLocalTransaction($transactionCode) {
        $transaction = Transaction::where('transaction_code', $transactionCode)->first();
        $transactionExist = Transaction::where('transaction_code', $transactionCode)->count();
        
        if ($transactionExist > 0) {
            return view('agro-commodity.process-local-transaction', compact('transaction', 'transactionCode'));
        } else {
           return redirect()->to('404');
        }
    }

    public function completeLocalTransaction(Request $request, $transactionCode) {
        
        // Mark notification as read if there is
        if ($request->input('ref') !== null && $request->input('key') !== null) {
            if ($request->input('ref') == 'nfn') {
                if (Notification::whereId($request->key)->count() == 1) {
                    $notification = Notification::find($request->key);
                    $notification->is_read = 1;
                    $notification->save();
                }
            }
        }

        $transaction = Transaction::where('transaction_code', $transactionCode)->first();
        $transactionExist = Transaction::where('transaction_code', $transactionCode)->count();
        
        if ($transactionExist > 0) {
            return view('agro-commodity.completed-local-transaction', compact('transaction', 'transactionCode'));
        } else {
           return redirect()->to('404');
        }
    }

    public function editLocalTransaction($transactionCode) {
        $transaction = Transaction::where('transaction_code', $transactionCode)->first();
        $transactionExist = Transaction::where('transaction_code', $transactionCode)->count();
        
        if ($transactionExist > 0) {
            return view('agro-commodity.edit-local-transaction', compact('transaction', 'transactionCode'));
        } else {
           return redirect()->to('404');
        }
    }

    public function myProfile() {
        return view('agro-commodity.my-profile');
    }
}
