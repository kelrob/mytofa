<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocalTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required',
            'volume' => 'required',
            'tonnage' => ['required', 'regex:/^[0-9,]+$/'],
            'quantity' => ['required', 'regex:/^[0-9,]+$/'],
            'price_per_kg' => ['required', 'regex:/^[0-9,]+$/'],
            'selling_price' => ['required', 'regex:/^[0-9,]+$/'],
            'paper_expenses' => ['required', 'regex:/^[0-9,]+$/'],
            'loading_expenses' => ['required', 'regex:/^[0-9,]+$/'],
            'accommodation_cost' => ['required', 'regex:/^[0-9,]+$/'],
            'transportation_cost' => ['required', 'regex:/^[0-9,]+$/'],
        ];
    }

    public function messages()
    {
        return [
            'tonnage.regex' => 'Please type in numbers from 0-9',
            'quantity.regex' => 'Please type in numbers from 0-9',
            'price_per_kg.regex' => 'Please type in numbers from 0-9',
            'selling_price.regex' => 'Please type in numbers from 0-9',
            'paper_expenses.regex' => 'Please type in numbers from 0-9',
            'loading_expenses.regex' => 'Please type in numbers from 0-9',
            'accomodation_cost.regex' => 'Please type in numbers from 0-9',
            'transportation_cost.regex' => 'Please type in numbers from 0-9',
        ];
    }
}
