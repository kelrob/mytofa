<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MultipleRolesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (
            Auth::check() && 
            (Auth::user()->role == 'super_admin' || Auth::user()->role == 'default' || Auth::user()->role == 'finance') 
            && Auth::user()->disabled != 1) {
            return $next($request);
        }
    }
}
