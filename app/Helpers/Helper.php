<?php

if (!function_exists('getDayTime')) {

    function getDayTime() {
        if (date("H") < 12) {
            return "morning";
        } elseif (date("H") > 11 && date("H") < 18) {
            return "afternoon";
        } elseif (date("H") > 17) {
            return "evening";
        }
    }
}


if (!function_exists('notificationExixt')) {
    
    function notificationExixt($userId) {
        $notificationCount = App\Notification::whereReceiverId($userId)->whereIsRead(0)->count();
        return $notificationCount > 0 ? true : false;
    }

}

if (!function_exists('getNotifications')) {

    function getNotifications($userId) {
        $notifications = App\Notification::whereReceiverId($userId)->orderBy('created_at', 'DESC')->limit(3)->get();

        return $notifications;
    }

}

if (!function_exists('truncate')) {
    function truncate($string, $length = 1000, $append = "...") {
        $string = trim($string);
      
        if(strlen($string) > $length) {
          $string = wordwrap($string, $length);
          $string = explode("\n", $string, 2);
          $string = $string[0] . $append;
        }
      
        return $string;
    }
}


if (!function_exists('getUserInfo')) {
    function getUserInfo($id, $value) {
        return App\User::find($id)->$value;
    }
}