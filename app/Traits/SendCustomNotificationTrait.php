<?php

namespace App\Traits;

use App\Notification;
use App\User;

trait SendCustomNotificationTrait {
    public function sendLocalNotification($receiverId, $senderId, $message, $transactionCode) {
    
        // New Notification
        $notification = new Notification();
        $notification->receiver_id = $receiverId;
        $notification->message = $message;
        $notification->is_read = 0;
        $notification->transaction_code = $transactionCode;
        $notification->save();

    }
}