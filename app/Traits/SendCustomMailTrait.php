<?php
namespace App\Traits;

use App\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

interface EmailDef {
    const EMAIL = 'emmanuel@tradersofafrica.com';
    const NAME = 'My TOFA';
}

trait SendCustomMailTrait {
    public function sendInviteMail($email) {

        $token = Crypt::encrypt($email);
        $user = User::where('email', $email)->first();

        $data = array(
            'name' => $user->name,
            'email' => $email,
            'token' => $token,
        );
        Mail::send('mail.invite', $data, function($message) use ($email) {
            $message->to($email)->subject
            ('MyTOFA Invitation');
            $message->from(EmailDef::EMAIL, EmailDef::NAME);
        });
    }

    public function sendPasswordChangedMail($email) {

        $token = Crypt::encrypt($email);
        $user = User::where('email', $email)->first();

        $data = array(
            'name' => $user->name,
            'email' => $email,
        );
        Mail::send('mail.password_changed', $data, function($message) use ($email) {
            $message->to($email)->subject
            ('Password Changed');
            $message->from(EmailDef::EMAIL, EmailDef::NAME);
        });
    }

    public function notifyAdminStartLocalTransaction($email, $financeEmail, $staffName) {

        $user = User::whereEmail($email)->first();
        $finance = User::whereEmail($financeEmail)->first();

        $data = array(
            'name' => $user->name,
            'staffName' => $staffName,
            'financeName' => $finance->name
        );

        // Notify Super Admin for start of Local Transaction
        Mail::send('mail.new_local_transaction', $data, function($message) use ($email, $staffName) {
            $message->to($email)
            ->subject("$staffName started a Local Transaction");
            
            $message->from(EmailDef::EMAIL, EmailDef::NAME);
        });

        // Notify Finance officer for start of Local Transaction
        Mail::send('mail.finance_local_transaction', $data, function($message) use ($email, $staffName, $financeEmail) {
            $message->to($financeEmail)
            ->subject("Local Transaction needs funding");
            
            $message->from(EmailDef::EMAIL, EmailDef::NAME);
        });
    }

    public function notifyStaffAndAdminFundedLocalTransaction($emails, $transactionCode) {
        foreach ($emails as $email) {
            $user = User::whereEmail($email)->first();
            
            $data = array(
                'transactionCode' => $transactionCode,
                'name' => $user->name
            );
            
            Mail::send('mail.funded_transaction', $data, function($message) use ($transactionCode, $email) {
                $message->to($email)
                ->subject("Transaction $transactionCode has been funded ");

                $message->from(EmailDef::EMAIL, EmailDef::NAME);
            });
        }
    }
}
